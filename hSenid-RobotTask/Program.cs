﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hSenid_RobotTask {
    class Program {
        static void Main(string[] args) {            
            SuperRobot superRobot = new SuperRobot();
            RobotRepo robotImpl = new RobotRepoImpl();

            while(true) {
                if(superRobot.life > 0) {
                    if(superRobot.strength <= 50) {
                        robotImpl.recharge(superRobot);
                        Console.WriteLine("Recharged");
                    } else {
                        Console.Write("strength: {0}, life: {1}\n", superRobot.strength, superRobot.life);
                        robotImpl.shoot(superRobot);
                        Console.Write("strength: {0}, life: {1}\n", superRobot.strength, superRobot.life);
                    }
                } else {
                    Console.WriteLine("Robot dead");
                    break;
                }
            }
            
            Console.ReadLine();
        }
    }
}
