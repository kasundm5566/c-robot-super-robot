﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hSenid_RobotTask {
    interface RobotRepo {
        void shoot(Robot robot);    
        void recharge(Robot robot);
    }
}
