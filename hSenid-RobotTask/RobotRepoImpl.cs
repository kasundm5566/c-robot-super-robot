﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hSenid_RobotTask {
    public class RobotRepoImpl : RobotRepo {

        void RobotRepo.recharge(Robot robot) {
            Random random = new Random();
            int currentStrength = robot.strength;
            int maxIncrement = robot.initialLife - currentStrength;
            int increment = random.Next(1, maxIncrement);
            robot.strength = currentStrength + increment;
        }

        void RobotRepo.shoot(Robot robot) {
            int k = robot.strength - 50;
            for(int i = 0; i < k; i++) {
                Console.Write("Shoot ");
            }
            Console.WriteLine();
            robot.strength -= k;
            robot.life -= 5;
        }
    }
}
