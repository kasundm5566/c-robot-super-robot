﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hSenid_RobotTask {
    public class Robot {

        public int initialLife { get;}
        public int strength { get; set; }
        public int life { get; set; }

        public Robot(int strength=75, int life=100) {
            initialLife = life;
            this.strength = strength;
            this.life = life;
        }
    }
}
